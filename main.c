#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fonctions.h"

int main(int argc, char **argv) {
  int option;
  int j;
  char **argvtab;

  Date date_Debut;
  Date date_Fin;

  float longitude_Max = 190.0;
  float longitude_Min = -190.0;
  float latitude_Max = 90.0;
  float latitude_Min = -90.0;

  FILE* fichier;
  FILE* fichier_ecriture;
  char ligne[1024];
  arbreAVL a = NULL;
  initialisationDate(&date_Debut, &date_Fin);

  // Copie des arguments d entree pour le getopt
  argvtab = malloc(argc * sizeof(char *));
  for (size_t i = 0; i < argc; i++) {

    argvtab[i] = argv[i];
  }

  // recuperation des donnes pour la date
  for (size_t i = 0; i < argc; i++) {
    if (strcmp("-d", argv[i]) == 0) {

      sscanf(argv[i + 1], "%d-%d-%d", &date_Debut.annee, &date_Debut.mois,&date_Debut.jour);
      sscanf(argv[i + 2], "%d-%d-%d", &date_Fin.annee, &date_Fin.mois,&date_Fin.jour);

    }
  }

  // recuperation des limites de zone
  for (size_t i = 0; i < argc; i++) {
    // initialisation des valeurs pour FRANCE
    if (strcmp("-F", argv[i]) == 0) {
      longitude_Max = 11.0;
      longitude_Min = -2.0;
      latitude_Max = 51.0;
      latitude_Min = 40.0;

    }
    // initialisation des valeurs pour GUYANE
    if (strcmp("-G", argv[i]) == 0) {
      longitude_Max = 51.0;
      longitude_Min = 55.0;
      latitude_Max = 7.0;
      latitude_Min = 2.0;
    }
    // initialisation des valeurs pour SAINT PIERRE ET MIQUELON
    if (strcmp("-S", argv[i]) == 0) {
      longitude_Max = 57.0;
      longitude_Min = 55.8;
      latitude_Max = 47.3;
      latitude_Min = 46.3;
    }
    // initialisation des valeurs pour ANTILLES
    if (strcmp("-A", argv[i]) == 0) {
      longitude_Max = -49.237995;
      longitude_Min = -71.468509;
      latitude_Max = 26.691649;
      latitude_Min = 7.424010;
    }
    // initialisation des valeurs pour OCEAN INDIEN
    if (strcmp("-O", argv[i]) == 0) {
      longitude_Max = 142.0;
      longitude_Min = 21.8383;
      latitude_Max = 26.3510;
      latitude_Min = -53.81;
    }
    // initialisation des valeurs pour ANTARTIQUE
    if (strcmp("-Q", argv[i]) == 0) {
      longitude_Max = 180.0;
      longitude_Min = -180.0;
      latitude_Max = -60.29;
      latitude_Min = -90.0;
    }
  }


  //ouverture du fichier
  for (size_t i = 0; i < argc; i++) {
    if (strcmp("-i", argv[i]) == 0) {
      j = i + 1;
      fichier = fopen(argv[j],"r");
    }
  }

  if (fichier == NULL) {

    aide();
    erreur(11);
  }



  while ((option = getopt(argc, argvtab, "tipwhmdAFGOSQ")) != -1) {

    switch (option) {
    case 't':
      switch (atoi(argv[optind])) {
      case 1:
            fgets(ligne,1024,fichier);
            fichier_ecriture = fopen("meteotemperaturemode1.dat","w");

            a = creerArbreTemperatureMode1(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

            afficherInfixeTemperatureMode1(a,fichier_ecriture);

        break;
      case 2:
            fgets(ligne,1024,fichier);

            fichier_ecriture = fopen("meteotemperaturemode2.dat","w");

            a = creerArbreTemperatureMode2(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

            afficherInfixeTemperatureMode2(a,fichier_ecriture);

        break;
      case 3:

        break;
      default:
        break;
      }
      break;
    case 'p':
      switch (atoi(argv[optind])) {
      case 1:
            fgets(ligne,1024,fichier);

            fichier_ecriture = fopen("meteopressionmode1.dat","w");
            a = creerArbrePressionMode1(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

            afficherInfixePressionMode1(a,fichier_ecriture);
        break;
      case 2:
            fgets(ligne,1024,fichier);

            fichier_ecriture = fopen("meteopressionmode2.dat","w");

            a = creerArbrePressionMode2(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

            afficherInfixePressionMode2(a,fichier_ecriture);

        break;
      case 3:

        break;
      default:
        break;
      }
      break;

    case 'w':
          fgets(ligne,1024,fichier);

          fichier_ecriture = fopen("meteovent.dat","w");

          a = creerArbreVent(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

          afficherInfixeVent(a,fichier_ecriture);

      break;
    case 'h':
          fgets(ligne,1024,fichier);

          fichier_ecriture = fopen("meteoaltitude.dat","w");

          a = creerArbreHauteur(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

          afficherInfixeHauteur(a,fichier_ecriture);

      break;
    case 'm':

          fgets(ligne,1024,fichier);

          fichier_ecriture = fopen("meteohumidite.dat","w");

          a = creerArbrehumidite(fichier,longitude_Max,longitude_Min,latitude_Max,latitude_Min,date_Debut,date_Fin);

          afficherInfixeHumidite(a,fichier_ecriture);

      break;
    default:
      break;
    }
  }

  return 0;
}
