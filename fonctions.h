#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef __fonctions_H_
#define __fonctions_H_




typedef struct Coordonnees Coordonnees;
struct Coordonnees{
    float latitude;
    float longitude;
};

typedef struct Date Date;
struct Date{
    int annee;
    int mois;
    int jour;
    int heure;
};

typedef struct Donnees Donnees;
struct Donnees{
    int* ID_Station;
    Date* date;
    int* pression_Niveau_Mer;
    int* direction_Moyen_Du_Vent;
    double* vitesse_Du_Vent;
    int* humidite;
    double* pression;
    double* variation_pression_24H;
    double* precipitation_24H;
    Coordonnees* coordonnees;
    double* temperature;
    double* Minimum;
    double* Maximum;
    int* altitude;
    int* communes;
    double* compteur;
    double* moyen;
    int* compteurV;
    int* moyenV;
};

// Structure d'un arbre AVL
typedef struct Noeud{
    Donnees* donnees;
    //CARACTERISTIQUE ARBRE AVL
    struct Noeud* filsG;
    struct Noeud* filsD;
    int hauteur;
}Noeud;
typedef Noeud* arbreAVL;


//FONCTION DE RECUPERATION DES DONNEES

int recuperationDesDonnees(FILE* fichier,Noeud* noeud, int ID_Station_flag, int Date_flag, int Presion_niveau_mer_flag, int direction_du_vent_flag,int vitesse_Du_Vent_flag, int Humidite_flag, int Pression_flag, int variation_pression_flag, int precipitation_flag, int Coordonnes_flag, int Temperature_flag, int Temperature_min_flag, int Temperature_max_flag, int altitude_flag, int communes_flag);

int initialisationDonnee(Noeud* noeud, int ID_Station_flag, int Date_flag, int Presion_niveau_mer_flag, int direction_du_vent_flag,int vitesse_Du_Vent_flag, int Humidite_flag, int Pression_flag, int variation_pression_flag, int precipitation_flag, int Coordonnes_flag, int Temperature_flag, int Temperature_min_flag, int Temperature_max_flag, int altitude_flag, int communes_flag);







/* Résumé : Fonctions permettant de faire un rotation a gauche*/
/* Entrée(s) : Un arbreAVL*/
/* Sortie(s) : Un arbreAVL*/
arbreAVL rotationGauche(arbreAVL a);



/* Résumé : Fonctions permettant de faire un rotation a droite*/
/* Entrée(s) : Un arbreAVL*/
/* Sortie(s) : Un arbreAVL*/
arbreAVL rotationDroite(arbreAVL a);


/* Résumé : Fonctions permettant de faire un rotation a droite puis a gauche*/
/* Entrée(s) : Un arbreAVL*/
/* Sortie(s) : Un arbreAVL*/
arbreAVL rotationDroiteGauche(arbreAVL a);



/* Résumé : Fonctions permettant de faire un rotation a gauche puis a droite*/
/* Entrée(s) : Un arbreAVL*/
/* Sortie(s) : Un arbreAVL*/
arbreAVL rotationGaucheDroite(arbreAVL a);






/* Résumé : Fonctions permettant d'afficher l'infixe d'un arbre*/
/* Entrée(s) : Un arbre*/
/* Sortie(s) : */
void afficherInfixeHumidite (arbreAVL a, FILE* fichier_ecriture);

// fonction permettant d'effectuer une insertion
arbreAVL creerArbrehumidite(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

// fonction permettant d'effectuer une insertion
arbreAVL insererHumidite(arbreAVL a, Noeud* b);

arbreAVL creerArbreTemperatureMode1(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

void afficherInfixeTemperatureMode1(arbreAVL a, FILE* fichier_ecriture);

arbreAVL insererTemperatureMode1(arbreAVL a, Noeud* b);

Noeud* creerNoeud(int hauteur, Donnees* donnee, Noeud* filsG, Noeud* filsD);

arbreAVL creerArbrePressionMode1(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

arbreAVL insererPressionMode1( arbreAVL a, Noeud* b);

void afficherInfixePressionMode1(arbreAVL a, FILE* fichier_ecriture);

void afficherInfixeHauteur (arbreAVL a, FILE* fichier_ecriture);

arbreAVL creerArbreHauteur(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

arbreAVL insererHauteur( arbreAVL a, Noeud* b);

arbreAVL creerArbreVent(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

arbreAVL insererVent( arbreAVL a, Noeud* b);

void afficherInfixeVent(arbreAVL a, FILE* fichier_ecriture);

arbreAVL creerArbrePressionMode2(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

arbreAVL insererPressionMode2( arbreAVL a, Noeud* b);

void afficherInfixePressionMode2(arbreAVL a, FILE* fichier_ecriture);

arbreAVL creerArbreTemperatureMode2(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin);

arbreAVL insererTemperatureMode2( arbreAVL a, Noeud* b);

void afficherInfixeTemperatureMode2(arbreAVL a, FILE* fichier_ecriture);

void initialisationDate(Date *date_Debut, Date *date_Fin);

void aide();

int erreur(int valeur);

int verificationDateEtCoordonnees(Noeud* b,float longitudeMax,float longitudeMin, float latitudeMax,float latitudeMin, Date date_Debut,Date date_Fin);


#endif
