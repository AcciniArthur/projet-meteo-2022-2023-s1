PROGRAMME C + AFFICHAGE GNUPLOT

Pour exécuter le programme, lancez le script météo.sh avec les différents modes et arguments. Par exemple :

	./meteo.sh -t 1 -t 2 --help -w 		va vous créer les graphiques de températures 					
                                        en mode 1 et 2 et le graphique du vent ,et vous 					
                                        afficher l’aide.

	./meteo.sh -h -A -d 2011-03-11 2015-06-24	
						va vous créer le graphique de hauteur pour la 						
                        zone des Antilles entre le 11 mars 2011 et le 24 						
                        juin 2015.

–help (double tiret) :
	Affiche l’aide détaillée du programme C

Température -t <mode>:
	mode 1 : on trie les ID de station de façon croissante, puis on rassemble toutes les 		température pour chaque station. Enfin on affiche la température minimum, 		maximum et moyenne par station.
	mode 2 : on trie les dates des relevés de façon chronologique puis on rassemble 			toutes les températures. Enfin, on affiche la température moyenne par 			station.
	mode 3 : on trie les dates des relevés de façon chronologique puis on trie les ID de 		station de façon croissante. Enfin, on affiche la température par heure/date 		par station.
	
Pression -p <mode> :
	mode 1 : on trie les ID de station de façon croissante, puis on rassemble toutes les 		pressions pour chaque station. Enfin on affiche la pression minimum, 			maximum et moyenne par station.
	mode 2 : on trie les dates des relevés de façon chronologique puis on rassemble 			toutes les pressions. Enfin, on affiche la pression moyenne par 				station.
	mode 3 : on trie les dates des relevés de façon chronologique puis on trie les ID de 		station de façon croissante. Enfin, on affiche la pression par heure/date 			par station.

affichage températures / pressions en mode 1 : Gnuplot affiche un diagramme de type barres d’erreur avec en abscisse l’identifiant de la station, et en ordonnée le minimum, maximum et la moyenne.

affichage températures / pressions en mode 2 : Gnuplot affiche un diagramme de type ligne simple avec en abscisse le jour et l’heure des mesures, et en ordonnée les moyenne des mesures.
affichage températures / pressions en mode 3 : Gnuplot affiche un diagramme de type multi-lignes avec en abscisse les jours, et en ordonnée les valeurs mesurées. Ce diagramme contient toutes les lignes, 1 par station et par heure.

Vent -w :
On trie les ID de station de façon croissante puis on fait la moyenne de l’orientation du vent et de la vitesse. Enfin on retourne les valeurs par ordre croissant de numéro de station.

affichage vent : Gnuplot affiche un diagramme de type vecteurs (flèches orientées) avec l’abscisse correspondant à la longitude (axe Ouest-Est) et l’ordonnée correspondant à la latitude (axe Nord-Sud).

Hauteur -h :
	On détermine la hauteur pour chaque station en les triant par ordre décroissant
affichage hauteur : Gnuplot affiche un diagramme de type carte interpolée et colorée avec l’abscisse correspondant à la longitude (axe Ouest-Est) et l’ordonnée correspondant à la latitude (axe Nord-Sud).

Humidité -m :
	On prend l’humidité maximum pour chaque station puis on affiche le résultat par ordre décroissant.

affichage humidité : Gnuplot affiche un diagramme de type carte interpolée et colorée avec l’abscisse correspondant à la longitude (axe Ouest-Est) et l’ordonnée correspondant à la latitude (axe Nord-Sud).

Longitude -g <min> <max> / Latitude -a <min> <max> :
	min et max sont des intervalles définie par l’utilisateur. La fonction permet de filtrer les valeurs de longitude et de latitude pour pouvoir se positionner sur une carte.

Dates -d <min> <max> :
	Cette fonction permet de filtrer les données de sortie qui sont dans l’intervalle de la date incluses.
		Format chaîne de type YYYY-MM-DD

Fichier d’entrée -i <nom_fichier> :
-A: Antilles :
permet de limiter les mesures à celles qui sont présentes aux Antilles.

-O : Océan indien :
permet de limiter les mesures à celles qui sont présentes dans l’océan
indien.

-Q : antarctique :
permet de limiter les mesures à celles qui sont présentes en Antarctique.
-d : dates <min> <max> :
permet de filtrer les dates entre les valeurs min et max, tout comme le ferait le programme C.

Les options -F, -G, -S, -A, -O, et -Q sont exclusives. Une seule option doit être activée à la fois. Si plusieurs options sont activées, un message d’erreur doit s’afficher. Si aucunes d’entre elles n’est activées, il n’y a aucune limite sur la position des relevés.
