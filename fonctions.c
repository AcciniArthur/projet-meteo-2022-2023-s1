#include "fonctions.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX(a,b) (a>b?a:b) // macro qui donne le maximum entre 2 nombres
#define MIN(a,b) (a<b?a:b) // macro qui donne le minimum entre 2 nombres


int hauteurAVL(arbreAVL a){
    if(a==NULL){ // s'il est vide
        return -1;
    } else{
        return a->hauteur; // sinon on met directement sa hauteur
    }
}


// permet de mesurer l'écart entre deux noeuds
int ecart(arbreAVL a){
  if(a==NULL){ // s'il est nul pas d'écart
        return 0;
    } else{
        return hauteurAVL(a->filsD)-hauteurAVL(a->filsG); // sinon on le calcule
    }
}
// fonction permettant d'équilibrer l'arbre afin qu'il puisse être un arbre AVL
arbreAVL equilibrer(arbreAVL a){
  if(ecart(a)==-2){
        if(ecart(a->filsG)==-1 || ecart(a->filsG)==0){
            a = rotationDroite(a);
        } else{
            a = rotationGaucheDroite(a);
        }
    } else if(ecart(a)==2){
        if(ecart(a->filsD)==1 || ecart(a->filsD)==0){
            a = rotationGauche(a);
        } else{
            a = rotationDroiteGauche(a);
        }
    } else{
        a->hauteur = MAX(hauteurAVL(a->filsG), hauteurAVL(a->filsD))+1;
    }
    return a;
}
// fonction permettant d'effectuer une rotation gauche afin d'équilibrer l'arbre
arbreAVL rotationGauche(arbreAVL a){
  Noeud* noeudTemp = a->filsD;
    a = creerNoeud(MAX(hauteurAVL(a->filsG), hauteurAVL(noeudTemp->filsG))+1, a->donnees, a->filsG, noeudTemp->filsG);
    return creerNoeud(MAX(hauteurAVL(a), hauteurAVL(noeudTemp->filsD))+1, noeudTemp->donnees, a, noeudTemp->filsD);
}
// fonction permettant d'effectuer une rotation droite afin d'équilibrer l'arbre
arbreAVL rotationDroite(arbreAVL a){
   Noeud* noeudTemp = a->filsG;
    a = creerNoeud(MAX(hauteurAVL(noeudTemp->filsD), hauteurAVL(a->filsD))+1, a->donnees, noeudTemp->filsD, a->filsD);
    return creerNoeud(MAX(hauteurAVL(noeudTemp->filsG), hauteurAVL(a))+1, noeudTemp->donnees, noeudTemp->filsG, a);
}
// fonction permettant d'effectuer une double rotation gauche afin d'équilibrer l'arbre
arbreAVL rotationDroiteGauche(arbreAVL a){
    a->filsD = rotationDroite(a->filsD);
    return rotationGauche(a);
}
// fonction permettant d'effectuer une double rotation gauche afin d'équilibrer l'arbre
arbreAVL rotationGaucheDroite(arbreAVL a){
    a->filsG = rotationGauche(a->filsG);
    return rotationDroite(a);
}
Noeud* creerNoeud(int hauteur, Donnees* donnee, Noeud* filsG, Noeud* filsD){
    /* on alloue un nouveau noeud et on lui met la bonne donnée */
    Noeud* noeudTemp = malloc(sizeof(Noeud));
    noeudTemp->hauteur = hauteur;
    noeudTemp->donnees = donnee;
    noeudTemp->filsG = filsG;
    noeudTemp->filsD = filsD;
    return noeudTemp;
}

int verificationDateEtCoordonnees(Noeud* b,float longitudeMax,float longitudeMin, float latitudeMax,float latitudeMin, Date date_Debut,Date date_Fin){
  int retour = 0;

  if (!(b->donnees->coordonnees->longitude < longitudeMax)) {
    retour = 1;
  }

  if (!(b->donnees->coordonnees->longitude > longitudeMin)) {
    retour = 1;
  }

  if (!(b->donnees->coordonnees->latitude < latitudeMax)) {
    retour = 1;
  }

  if (!(b->donnees->coordonnees->latitude > latitudeMin)) {
    retour = 1;
  }

  return retour;
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// permet de retourner un arbre dans l'ordre
void afficherInfixeHumidite (arbreAVL a, FILE* fichier_ecriture){
  if (a != NULL) {
    afficherInfixeHumidite(a->filsD, fichier_ecriture);
     fprintf(fichier_ecriture, "%f %f %d\n", a->donnees->coordonnees->longitude,a->donnees->coordonnees->latitude, *a->donnees->humidite);
    afficherInfixeHumidite(a->filsG, fichier_ecriture);
  }
}
// fonction permettant d'effectuer une insertion
arbreAVL creerArbrehumidite(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0);

    if ( (res == 0) && verif == 0) {
        a = insererHumidite(a, b);
    }else {
      free(b);
    }
  }
  return a;
}


arbreAVL insererHumidite( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
    if (*b->donnees->ID_Station < *a->donnees->ID_Station) {
      a->filsG = insererHumidite( a->filsG, b);
    }else if (*b->donnees->ID_Station > *a->donnees->ID_Station) {
      a->filsD = insererHumidite( a->filsD, b);
    }else{
        if (*b->donnees->humidite > *a->donnees->humidite){
          a->donnees->humidite = b->donnees->humidite;
        }
      }
  }
  return equilibrer(a);
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


arbreAVL creerArbreTemperatureMode1(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0);

    if ( (res == 0) && verif == 0) {
      b->donnees->compteur = malloc(1*sizeof(double));
      b->donnees->moyen = malloc(1*sizeof(double));
      b->donnees->Maximum = malloc(1*sizeof(double));
      b->donnees->Minimum = malloc(1*sizeof(double));
      *b->donnees->compteur = 1.0;
      *b->donnees->moyen = *b->donnees->temperature;
      *b->donnees->Maximum = *b->donnees->temperature;
      *b->donnees->Minimum = *b->donnees->temperature;
      a = insererTemperatureMode1(a, b);
    }else {
      free(b);
    }
  }
  return a;
}

// fonction permettant d'effectuer une insertion
//Il manque l'insertion de la temperature (moyenne, max et min)
arbreAVL insererTemperatureMode1( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
    if (*b->donnees->ID_Station < *a->donnees->ID_Station) {
      a->filsG = insererTemperatureMode1( a->filsG, b);

    }else{
    if (*b->donnees->ID_Station > *a->donnees->ID_Station) {
      a->filsD = insererTemperatureMode1( a->filsD, b);
    }
    else{
      if (*b->donnees->temperature > *a->donnees->Maximum){
        a->donnees->Maximum = b->donnees->temperature;
      }
      if (*b->donnees->temperature < *a->donnees->Minimum){
        a->donnees->Minimum = b->donnees->temperature;
      }
      *a->donnees->compteur = *a->donnees->compteur + 1.0;
      *a->donnees->moyen = *a->donnees->moyen + *b->donnees->temperature;
        }
      }
    }
    return equilibrer(a);
  }


// permet de retourner un arbre dans l'ordre
//Il manque l'insertion de la temperature (moyenne, max et min)
void afficherInfixeTemperatureMode1(arbreAVL a, FILE* fichier_ecriture){
  double calc;
  if (a != NULL) {
    afficherInfixeTemperatureMode1(a->filsG, fichier_ecriture);
    calc = *a->donnees->moyen / *a->donnees->compteur;
    fprintf(fichier_ecriture, "%d %lf %lf %lf\n", *a->donnees->ID_Station,*a->donnees->Minimum, *a->donnees->Maximum, calc);
    afficherInfixeTemperatureMode1(a->filsD, fichier_ecriture);
    }
  }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  arbreAVL creerArbreTemperatureMode2(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
    Noeud* a = NULL;
    while (!feof(fichier)) {
      Noeud* b = malloc(sizeof(Noeud)*1);
      b->filsG = NULL;
      b->filsD = NULL;
      b->hauteur = 0;
      int verif = 0;
      int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0);
      if ( (res == 0) && verif == 0) {
          b->donnees->compteur = malloc(1*sizeof(double));
          b->donnees->moyen = malloc(1*sizeof(double));
          *b->donnees->compteur = 1.0;
          *b->donnees->moyen = *b->donnees->temperature;
          a = insererTemperatureMode2(a, b);

      }else{
        free(b);

        }
      }
      return a;
    }
  // fonction permettant d'effectuer une insertion
  //Il manque l'insertion de la temperature (moyenne, max et min)
  arbreAVL insererTemperatureMode2( arbreAVL a, Noeud* b){
    if (a == NULL) {
      return b;
    }else{
        if (b->donnees->date->annee < a->donnees->date->annee) {
          a->filsG = insererTemperatureMode2( a->filsG, b);
        }else if (b->donnees->date->annee > a->donnees->date->annee){
            a->filsD = insererTemperatureMode2( a->filsD, b);
        }else{
          if (b->donnees->date->mois < a->donnees->date->mois) {
            a->filsG = insererTemperatureMode2( a->filsG, b);
          }else if (b->donnees->date->mois > a->donnees->date->mois){
              a->filsD = insererTemperatureMode2( a->filsD, b);
          }else{
            if (b->donnees->date->jour < a->donnees->date->jour) {
              a->filsG = insererTemperatureMode2( a->filsG, b);
            }else if (b->donnees->date->jour > a->donnees->date->jour){
                a->filsD = insererTemperatureMode2( a->filsD, b);
            }else{
              if (b->donnees->date->heure < a->donnees->date->heure) {
                a->filsG = insererTemperatureMode2( a->filsG, b);
              }else if (b->donnees->date->heure > a->donnees->date->heure){
                  a->filsD = insererTemperatureMode2( a->filsD, b);
              }else{
                *a->donnees->compteur = *a->donnees->compteur + 1.0;
                *a->donnees->moyen = *a->donnees->moyen + *b->donnees->temperature;
              }
            }
          }
        }
      }
    return equilibrer(a);
  }


  // permet de retourner un arbre dans l'ordre
  //Il manque l'insertion de la temperature (moyenne, max et min)
  void afficherInfixeTemperatureMode2(arbreAVL a, FILE* fichier_ecriture){
    double calc;
    if (a != NULL) {
      afficherInfixeTemperatureMode2(a->filsG, fichier_ecriture);
      if (*a->donnees->moyen != 0){
        calc = *a->donnees->moyen / *a->donnees->compteur;
        fprintf(fichier_ecriture, "%d-%d-%d/%d %lf\n", a->donnees->date->annee,a->donnees->date->mois,a->donnees->date->jour,a->donnees->date->heure, calc);
      }
      afficherInfixeTemperatureMode2(a->filsD, fichier_ecriture);
    }
  }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

arbreAVL creerArbrePressionMode1(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0);
    if ( (res == 0) && verif == 0) {
        b->donnees->compteur = malloc(1*sizeof(double));
        b->donnees->moyen = malloc(1*sizeof(double));
        b->donnees->Maximum = malloc(1*sizeof(double));
        b->donnees->Minimum = malloc(1*sizeof(double));
        *b->donnees->compteur = 1.0;
        *b->donnees->moyen = *b->donnees->pression;
        *b->donnees->Maximum = *b->donnees->pression;
        *b->donnees->Minimum = *b->donnees->pression;
        a = insererPressionMode1(a, b);

    }else{
      free(b);

      }
    }
    return a;
  }

// fonction permettant d'effectuer une insertion
//Il manque l'insertion de la temperature (moyenne, max et min)
arbreAVL insererPressionMode1( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
    if (*b->donnees->ID_Station < *a->donnees->ID_Station) {
      a->filsG = insererPressionMode1( a->filsG, b);

    }else{
    if (*b->donnees->ID_Station > *a->donnees->ID_Station) {
      a->filsD = insererPressionMode1( a->filsD, b);
    }
    else{
      if (*b->donnees->pression > *a->donnees->Maximum){
        a->donnees->Maximum = b->donnees->pression;
      }
      if (*b->donnees->pression < *a->donnees->Minimum){
        if (*b->donnees->pression != 0) {
          a->donnees->Minimum = b->donnees->pression;
        }
      }
      *a->donnees->compteur = *a->donnees->compteur + 1.0;
      *a->donnees->moyen = *a->donnees->moyen + *b->donnees->pression;
        }
      }
    }
    return equilibrer(a);
  }


// permet de retourner un arbre dans l'ordre
//Il manque l'insertion de la temperature (moyenne, max et min)
void afficherInfixePressionMode1(arbreAVL a, FILE* fichier_ecriture){
  double calc;
  if (a != NULL) {
    afficherInfixePressionMode1(a->filsG, fichier_ecriture);
    if (*a->donnees->Minimum != 0){
      calc = *a->donnees->moyen / *a->donnees->compteur;
    fprintf(fichier_ecriture, "%d %lf %lf %lf\n", *a->donnees->ID_Station,*a->donnees->Minimum, *a->donnees->Maximum, calc);
    }
  afficherInfixePressionMode1(a->filsD, fichier_ecriture);
  }
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

arbreAVL creerArbrePressionMode2(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0);
    if ( (res == 0) && verif == 0) {
        b->donnees->compteur = malloc(1*sizeof(double));
        b->donnees->moyen = malloc(1*sizeof(double));
        *b->donnees->compteur = 1.0;
        *b->donnees->moyen = *b->donnees->pression;
        a = insererPressionMode2(a, b);

    }else{
      free(b);

      }
    }
    return a;
  }
// fonction permettant d'effectuer une insertion
//Il manque l'insertion de la temperature (moyenne, max et min)
arbreAVL insererPressionMode2( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
      if (b->donnees->date->annee < a->donnees->date->annee) {
        a->filsG = insererPressionMode2( a->filsG, b);
      }else if (b->donnees->date->annee > a->donnees->date->annee){
          a->filsD = insererPressionMode2( a->filsD, b);
      }else{
        if (b->donnees->date->mois < a->donnees->date->mois) {
          a->filsG = insererPressionMode2( a->filsG, b);
        }else if (b->donnees->date->mois > a->donnees->date->mois){
            a->filsD = insererPressionMode2( a->filsD, b);
        }else{
          if (b->donnees->date->jour < a->donnees->date->jour) {
            a->filsG = insererPressionMode2( a->filsG, b);
          }else if (b->donnees->date->jour > a->donnees->date->jour){
              a->filsD = insererPressionMode2( a->filsD, b);
          }else{
            if (b->donnees->date->heure < a->donnees->date->heure) {
              a->filsG = insererPressionMode2( a->filsG, b);
            }else if (b->donnees->date->heure > a->donnees->date->heure){
                a->filsD = insererPressionMode2( a->filsD, b);
            }else{
              *a->donnees->compteur = *a->donnees->compteur + 1.0;
              *a->donnees->moyen = *a->donnees->moyen + *b->donnees->pression;
            }
          }
        }
      }
    }
  return equilibrer(a);
}


// permet de retourner un arbre dans l'ordre
//Il manque l'insertion de la temperature (moyenne, max et min)
void afficherInfixePressionMode2(arbreAVL a, FILE* fichier_ecriture){
  double calc;
  if (a != NULL) {
    afficherInfixePressionMode2(a->filsG, fichier_ecriture);
    if (*a->donnees->moyen != 0){
      calc = *a->donnees->moyen / *a->donnees->compteur;
      fprintf(fichier_ecriture, "%d-%d-%d/%d %lf\n", a->donnees->date->annee,a->donnees->date->mois,a->donnees->date->jour,a->donnees->date->heure, calc);
    }
    afficherInfixePressionMode2(a->filsD, fichier_ecriture);
  }
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

arbreAVL creerArbreVent(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
    if ( (res == 0) && verif == 0) {
        b->donnees->compteur = malloc(1*sizeof(double));
        b->donnees->moyen = malloc(1*sizeof(double));
        b->donnees->compteurV = malloc(1*sizeof(int));
        b->donnees->moyenV = malloc(1*sizeof(int));
        *b->donnees->compteur = 1.0;
        *b->donnees->moyen = *b->donnees->vitesse_Du_Vent;
        *b->donnees->compteurV = 1;
        *b->donnees->moyenV = *b->donnees->direction_Moyen_Du_Vent;
        a = insererVent(a, b);

    }else{
      free(b);

      }
    }
    return a;
  }
// fonction permettant d'effectuer une insertion
//Il manque l'insertion de la temperature (moyenne, max et min)
arbreAVL insererVent( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
    if (*b->donnees->ID_Station < *a->donnees->ID_Station) {
      a->filsG = insererVent( a->filsG, b);

    }else{
    if (*b->donnees->ID_Station > *a->donnees->ID_Station) {
      a->filsD = insererVent( a->filsD, b);
    }
    else{
      *a->donnees->compteur = *a->donnees->compteur + 1.0;
      *a->donnees->moyen = *a->donnees->moyen + *b->donnees->vitesse_Du_Vent;
      *a->donnees->compteurV = *a->donnees->compteurV + 1;
      *a->donnees->moyenV = *a->donnees->moyenV + *b->donnees->direction_Moyen_Du_Vent;
        }
      }
    }
    return equilibrer(a);
  }


// permet de retourner un arbre dans l'ordre
//Il manque l'insertion de la temperature (moyenne, max et min)
void afficherInfixeVent(arbreAVL a, FILE* fichier_ecriture){
  double calc;
  int calc2;
  if (a != NULL) {
    afficherInfixeVent(a->filsG, fichier_ecriture);
    calc = *a->donnees->moyen / *a->donnees->compteur;
    calc2 = *a->donnees->moyenV / *a->donnees->compteurV;
    fprintf(fichier_ecriture, "%f %f %lf %d \n",a->donnees->coordonnees->longitude,a->donnees->coordonnees->latitude,calc,calc2 );
    afficherInfixeVent(a->filsD, fichier_ecriture);
  }
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// permet de retourner un arbre dans l'ordre
void afficherInfixeHauteur (arbreAVL a, FILE* fichier_ecriture){
  if (a != NULL) {
    afficherInfixeHauteur(a->filsD, fichier_ecriture);
    fprintf(fichier_ecriture, "%f %f %d \n",a->donnees->coordonnees->longitude,a->donnees->coordonnees->latitude,*a->donnees->altitude);
    afficherInfixeHauteur(a->filsG, fichier_ecriture);
  }
}

// fonction permettant d'effectuer une insertion
arbreAVL creerArbreHauteur(FILE* fichier,float longitude_Max,float longitude_Min,float latitude_Max,float latitude_Min,Date date_Debut,Date date_Fin){
  Noeud* a = NULL;
  while (!feof(fichier)) {
    Noeud* b = malloc(sizeof(Noeud)*1);
    b->filsG = NULL;
    b->filsD = NULL;
    b->hauteur = 0;
    int verif = 0;
    int res = recuperationDesDonnees(fichier, b, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0);
    if ( (res == 0) && verif == 0) {
      a = insererHauteur(a, b);
    }else{
      free(b);

    }
  }
  return a;
}


arbreAVL insererHauteur( arbreAVL a, Noeud* b){
  if (a == NULL) {
    return b;
  }else{
    if (*b->donnees->altitude < *a->donnees->altitude) {
      a->filsG = insererHauteur( a->filsG, b);
    }else if (*b->donnees->altitude > *a->donnees->altitude) {
      a->filsD = insererHauteur( a->filsD, b);
    }
    return equilibrer(a);
  }
}









//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





int recuperationDesDonnees(FILE* fichier,Noeud* noeud, int ID_Station_flag, int Date_flag, int presion_Niveau_Mer_flag, int direction_Moyen_Du_Vent_flag,int vitesse_Du_Vent_flag, int humidite_flag, int pression_flag, int variation_pression_24H_flag, int precipitation_24H_flag, int coordonnees_flag, int temperature_flag, int temperature_Minimum_flag, int temperature_Maximum_flag, int altitude_flag, int communes_flag){

    char ligne[1024];
    char* jeton;
    char* copie;
    int res;

    res = initialisationDonnee(noeud,ID_Station_flag,Date_flag,presion_Niveau_Mer_flag,direction_Moyen_Du_Vent_flag,vitesse_Du_Vent_flag,humidite_flag,pression_flag,variation_pression_24H_flag,precipitation_24H_flag,coordonnees_flag,temperature_flag,temperature_Minimum_flag,temperature_Maximum_flag,altitude_flag,communes_flag);

    //MISE EN VARIABLE DES VALEUR VOULU
    fgets(ligne,1024,fichier);
    copie = strdup(ligne);
    jeton = strsep(&copie,";");
    if(ID_Station_flag){
          if (jeton == NULL)
          {
            res =1;
          }else{
            *noeud->donnees->ID_Station = atoi(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (Date_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            sscanf(jeton,"%d-%d-%dT%d:",&noeud->donnees->date->annee,&noeud->donnees->date->mois,&noeud->donnees->date->jour,&noeud->donnees->date->heure);
          }
        }

    jeton = strsep(&copie,";");
    if (presion_Niveau_Mer_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->pression_Niveau_Mer = atoi(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (direction_Moyen_Du_Vent_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->direction_Moyen_Du_Vent = atoi(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (vitesse_Du_Vent_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->vitesse_Du_Vent = atof(jeton);
            }
        }

    jeton = strsep(&copie,";");
    if (humidite_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->humidite = atoi(jeton);
            }
        }

    jeton = strsep(&copie,";");
    if (pression_flag)
        {
          if(jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->pression = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (variation_pression_24H_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
          *noeud->donnees->variation_pression_24H = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (precipitation_24H_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->precipitation_24H = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (coordonnees_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            sscanf(jeton,"%f,%f",&noeud->donnees->coordonnees->latitude,&noeud->donnees->coordonnees->longitude);
          }
        }

    jeton = strsep(&copie,";");
    if (temperature_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->temperature = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (temperature_Minimum_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->Minimum = atof(jeton);
            }
        }

    jeton = strsep(&copie,";");
    if (temperature_Maximum_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->Maximum = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (altitude_flag)
        {
          if (jeton == NULL)
          {
            res = 1;
          }else{
            *noeud->donnees->altitude = atof(jeton);
          }
        }

    jeton = strsep(&copie,";");
    if (communes_flag)
        {
          if (jeton == NULL)
          {
            res =1;
          }else{
            *noeud->donnees->communes = atoi(jeton);
          }
        }

    return res;
  }





int initialisationDonnee(Noeud* noeud, int ID_Station_flag, int Date_flag, int presion_Niveau_Mer_flag, int direction_Moyen_Du_Vent_flag,int vitesse_Du_Vent_flag, int humidite_flag, int pression_flag, int variation_pression_24H_flag, int precipitation_24H_flag, int coordonnees_flag, int temperature_flag, int temperature_Minimum_flag, int temperature_Maximum_flag, int altitude_flag, int communes_flag){

    noeud->donnees=malloc(1*(sizeof(Donnees)));

    if(ID_Station_flag){
        noeud->donnees->ID_Station = (int*)malloc(sizeof(int));
    }

    if(Date_flag){
    noeud->donnees->date = malloc(1*sizeof(Date));
    }

    if(presion_Niveau_Mer_flag){
    noeud->donnees->pression_Niveau_Mer = malloc(1*sizeof(int));
    }

    if(direction_Moyen_Du_Vent_flag){
    noeud->donnees->direction_Moyen_Du_Vent = malloc(1*sizeof(int));

    }

    if (vitesse_Du_Vent_flag){
      noeud->donnees->vitesse_Du_Vent = malloc(1*sizeof(double));

    }

    if(humidite_flag){
        noeud->donnees->humidite = malloc(1*sizeof(int));
    }

    if(pression_flag){
        noeud->donnees->pression = malloc(1*sizeof(double));

    }

    if(variation_pression_24H_flag){
        noeud->donnees->variation_pression_24H =malloc(1*sizeof(double));
    }

    if(precipitation_24H_flag){
        noeud->donnees->precipitation_24H = malloc(1*sizeof(double));
    }
    ;
    if(coordonnees_flag){
        noeud->donnees->coordonnees = malloc(1*sizeof(char));
    }

    if(temperature_flag){
        noeud->donnees->temperature = malloc(1*sizeof(double));
    }

    if(temperature_Minimum_flag){
        noeud->donnees->Minimum = malloc(1*sizeof(double));
    }

    if(temperature_Maximum_flag){
        noeud->donnees->Maximum = malloc(1*sizeof(double));
    }

    if(altitude_flag){
        noeud->donnees->altitude = malloc(1*sizeof(double));
    }

    if(communes_flag){
        noeud->donnees->communes = malloc(1*sizeof(int));
    }

    return 0;
}


void initialisationDate(Date *date_Debut, Date *date_Fin) {
  date_Debut->annee = 2001;
  date_Debut->mois = 7;
  date_Debut->jour = 11;
  date_Debut->heure = 11;

  date_Fin->annee = 2023;
  date_Fin->mois = 2;
  date_Fin->jour = 14;
  date_Fin->heure = 1;
}

void aide(){

    printf("Voici l'aide à l'utilisation du programme C\n\n\n");
    printf("\n------------------------------------------------\n");
    printf("-t comprend 3 modes :\n");
    printf("le mode 1 permet de d'afficher la température minimum, maximum et moyenne par station.\n");
    printf("le mode 2 permet d'afficher la température moyenne par station de manière chrnonologique.\n");
    printf("le mode 3 permet d'afficher la température par heure/date et par station.\n\n");
    printf("\n------------------------------------------------\n");
    printf("-p comprend 3 modes :\n");
    printf("le mode 1 permet de d'afficher la pression minimum, maximum et moyenne par station.\n");
    printf("le mode 2 permet d'afficher la pression moyenne par station de manière chrnonologique.\n");
    printf("le mode 3 permet d'afficher la pression par heure/date et par station.\n\n");
    printf("\n------------------------------------------------\n");
    printf("-w permet de retourner la moyenne de l'orientation du vent et de la vitesse par ordre croissant de numéro de station.\n\n");
    printf("\n------------------------------------------------\n");
    printf("-h permet de retourner la hauteur de chaque station par ordre décroissant.\n\n");
    printf("\n------------------------------------------------\n");
    printf("-m permet de retourner l'humidité maximum pour chaque station par ordre décroissant. \n\n");
    printf("\n------------------------------------------------\n");
    printf("-g permet de définir une longitude et une latitude min et max comme argument de fonction. \n\n");
    printf("\n------------------------------------------------\n");
    printf("-d permet de filtrer les données dans l'intervalle de la date fournie. Le format est YYYY-MM-DD\n\n");
    printf("\n------------------------------------------------\n");
    printf("-i permet de spécifier que le fichier d'entrée est un .csv. \n\n");
    printf("\n------------------------------------------------\n");
    printf("-o permet de donner un nom au fichier de sortie. \n\n");
    printf("\n------------------------------------------------\n");
    printf("-g, -a et -d sont optionnelles. \n\n");
    printf("\n------------------------------------------------\n");
    printf("-t, -p, -w, -h et -m sont exclusives.\n\n");

}

int erreur(int valeur) {
  switch (valeur) {
  case 10:
    printf("Le fichier n'existe pas\n");
    exit(10);
    break;
  case 11:

    break;
  default:

    break;
  }
}
