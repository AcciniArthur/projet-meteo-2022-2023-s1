#!/bin/bash
cat -n "meteotemperaturemode1.dat" | sed -e 's/     //g' -e 's/    //g' -e 's/\t/ /g' > temp.dat
gnuplot <<-EOF
  set style data errorbars
  set xtics rotate by -45
  set xlabel "Identifiant de la station"
  set ylabel "Valeur"
  set yrange [0:100]
  set terminal png size 2400,1080
  set datafile separator " "
  set output "temperature_mode1.png"
  plot "temp.dat" using 1:5:xtic(2) with l
EOF
