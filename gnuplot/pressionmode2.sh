#!/bin/bash
gnuplot <<-EOF
    set xdata time
    set timefmt "%Y-%m-%d/%H"
    set xlabel "Jour et Heure"
    set ylabel "Moyenne des mesures"
    set terminal png size 9120,1080
    set autoscale xfixmin
    set autoscale xfixmax
    set xtics 3600*24*30
    set xtics rotate by 90
    set size 0.95,0.95
    set origin 0.025,0.025
    set xtics format "%b %d %Y"
    set datafile separator " "
    set output "pression_mode2.png"
    plot "meteopressionmode2.dat" using 1:2 with lines
EOF
