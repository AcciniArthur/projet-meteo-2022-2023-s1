#!/bin/bash

gnuplot <<-EOFMarker
  set terminal png
  set autoscale xfix
  set autoscale yfix
  set border linewidth 8
  set pm3d map
  set dgrid3d 500,500,2
  set xlabel "Longitude"
  set ylabel "Latitude"
  set title "Carte interpolée et colorée d'humidité"
  set datafile separator " "
  set output "hauteur.png"
  splot "meteoaltitude.dat" u 1:2:3 with pm3d
EOFMarker
