#!/bin/bash
gnuplot <<-EOF
  set xrange [-180:180]
  set yrange [-90:90]
  set xlabel "Longitude (Ouest-Est)"
  set ylabel "Latitude (Nord-Sud)"
  set arrow from 0,0 to 30,40 size screen 0.03,15,45 filled
  set arrow from 10,20 to 40,50 size screen 0.03,15,45 filled
  set terminal png size 14400,7200
  set datafile separator " "
  set output "vent.png"
  plot "meteovent.dat" using 1:2:(\$3):(-\$4+90) with arrows
EOF
