#!/usr/bin/bash

###############################
#VARIABLE GLOBAL
#changer le nom du fichier afin de traiter le bon fichier
fichier="meteo_filtered_data_v1.csv"
###############################



#
#FONCTIONS
#

#FONCTION COMPILATION
#Compile si l'executable du C n'existe pas
compilation (){
	if [ ! -f exe.exe ]
    then
			gcc -c main.c -o main.o
			gcc -c fonctions.c -o fonctions.o
      gcc fonctions.o main.o -o exe
    fi
}

#FONCTION ERREUR
#gere les differents code d'erreur
erreur (){
    case "$1" in
        1)  #TROP D ARGUEMENT EXCLUSIVE DE TYPE -A -F -O
            ./exe --help
            echo "!!!Echec de lancement!!!: TROP D ARGUEMENT EXCLUSIVE DE TYPE LOCALISATION"
            exit 1
        ;;
        2)  #MODE NON DISPONIBLE POUR TEMPERATURE ET PRESION
            ./exe --help
            echo "!!!Echec de lancement!!!: MODE D'EXECUTION INCONNUE, CHOISIR 1,2 ou 3"
            exit 1
        ;;
        3)  #OPTION NON DISPONIBLE
            ./exe --help
            echo "!!!Echec de lancement!!!: OPTION INDISPONIBLE"    #TODO: Rajoutez quelle option est indisponible
            exit 1
        ;;
        *) #ERREUR PAR DEFAUT
            ./exe --help
            exit 1
        ;;
    esac
}






#
#DEBUT DE L'EXECUTION
#


compilation

#TODO: CHANGER LA LIGNE POUR EXECUTER SI --help
#Verifie si --help est dans la ligne de commande
#si present execute le C avec --help comme argument
#puis decale --help en 1er argument et le supprime avec shift
    aide="--help"
    for i in $(seq 0 $#)
        do
            if [ "$aide" = "${!i}" ]
            then
            ./exe --help
            let "j = $i - 1"
            let "k = $i + 1"
            set -- "${!i}" "${@:1:j}" "${@:k}" #decalage de --help
            shift
            unset j
            unset k
        fi
    done

#GESTION DES OPTIONS TEL QUE -F -G -S -A -O -Q -D
compteur_d_option=0
nom_de_la_localisation=""
A="-A"
F="-F"
G="-G"
O="-O"
S="-S"
Q="-Q"
for i in $(seq 0 $#)
do
    case "${!i}" in
        -A)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
            nom_de_la_localisation="-A"
        ;;
        -F)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
            nom_de_la_localisation="F"
        ;;
        -G)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
            nom_de_la_localisation="-G"
        ;;
        -O)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
            nom_de_la_localisation="-O"
        ;;
        -S)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
            nom_de_la_localisation="-S"
        ;;
        -Q)
            let "compteur_d_option = $compteur_d_option + 1"
            unset nom_de_la_localisation
           nom_de_la_localisation="-Q"
        ;;
        *)
        ;;
    esac
done


if [ "$compteur_d_option" -ge "2"  ]
then
    erreur 1
fi

#GESTION DE L'OPTION DATE
date=""
for i in $(seq 0 $#)
do
    if [ "${!i}" = "-d" ]
    then
    let "j= $i +1"
    let "k= $i +2"
    date_debut="${!j}"
    date_fin="${!k}"
    unset date
    date="-d ${date_debut} ${date_fin}"
    fi
done

#Gestion des arguments de la ligne de commande

    while getopts t:p:whmwAFGOSQ OPTION
    do
        case ${OPTION} in
            t)
                case "$OPTARG" in
                    1)
                       ./exe -i $fichier -t 1 $nom_de_la_localisation $date
						./gnuplot/temperaturemode1.sh
                    ;;
                    2)
                       ./exe -i $fichier -t 2 $nom_de_la_localisation $date
						./gnuplot/temperaturemode2.sh
                    ;;
                    3)
                        ./exe -i $fichier -t 3 $nom_de_la_localisation $date
                    ;;
                    *)
                        erreur 2
                    ;;
                esac
            ;;
            p)
                case "$OPTARG" in
                    1)
                    	./exe -i $fichier -p 1 $nom_de_la_localisation $date
						./gnuplot/pressionmode1.sh
                    ;;
                    2)
                        ./exe -i $fichier -p 1 $nom_de_la_localisation $date
						./gnuplot/pressionmode2.sh
                    ;;
                    3)
                        ./exe -i $fichier -p 1 $nom_de_la_localisation $date
                    ;;
                esac
            ;;
            w)
                ./exe -i $fichier -w $nom_de_la_localisation $date
				./gnuplot/vent.sh
            ;;
            h)
		        ./exe -i $fichier -h $nom_de_la_localisation $date
				./gnuplot/hauteur.sh
            ;;
            m)
                ./exe -i $fichier -m $nom_de_la_localisation $date
				./gnuplot/humidite.sh
            ;;
            d)
            ;;
						A)
            ;;
						F)
            ;;
						G)
            ;;
						O)
            ;;
						S)
            ;;
						Q)
            ;;
            *)
                erreur 3
            ;;
					esac
			done






rm -f *.o
rm -f exe
